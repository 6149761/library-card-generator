<?php

//Don't cache the site or weird things happen.
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

//User input.
$name = $_POST['name'];
$cardnumber = $_POST['cardnumber'];
$barcode  = "*".$cardnumber."*";

//Prevent duplicates and collisions.
$tstamp = date("Ymd-His"); //Timestamp
$rando = rand(100, 999); //Random number
$imgname = "name-{$rando}-{$tstamp}.png";
$imgcardnumber = "card-{$rando}-{$tstamp}.png";
$imgbarcode = "barcode-{$rando}-{$tstamp}.png";
$librarycard = "MiskatonicLibrary.jpg";
$finalcard = "{$tstamp}-{$rando}-{$librarycard}";


//Debug stuff - turn on as needed.
//echo $name.'<br>';
//echo $cardnumber.'<br>';
//echo $barcode.'<br>';
//echo $tstamp.'<br>';
//echo $rando.'<br>';
//echo $imgname.'<br>';
//echo $imgcardnumber.'<br>';
//echo $imgbarcode.'<br>';
//echo $finalcard.'<br>';


exec("convert -background none -fill black -font \"Sunshine-In-My-Soul\" -size 2060x500 -gravity west caption:\"".$name."\" $imgname");

exec("convert -background none -fill black -font \"Free-3-of-9-Regular\" -size 1250x600 caption:\"".$barcode."\" $imgbarcode");

exec("convert -background none -fill black -size 600x100 caption:\"".$cardnumber."\" $imgcardnumber");

exec("convert -quality 80% -background white -size 3000x1980 -page +0+0 MiskatonicCardBase.png -page +850+1460 $imgname -page +1250+900 $imgbarcode -page +1575+1150 $imgcardnumber -layers flatten $finalcard");

unlink($imgname);
unlink($imgcardnumber);
unlink($imgbarcode);

?>

<br>
<img src="<?=$finalcard?>" width="700">